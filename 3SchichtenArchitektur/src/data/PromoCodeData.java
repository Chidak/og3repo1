package data;

import java.util.LinkedList;
import java.util.List;

import java.io.*;

public class PromoCodeData implements IPromoCodeData {

	private List<String> promocodes;
	
	public PromoCodeData(){
		this.promocodes = new LinkedList<String>();
	}
	
	// Schreibt PromoCode in die verkettete Liste "promocodes" falls erfolgreich in der Liste gespeichert 
	// --> speicher ihn in der Datei falls nicht gebe false zur�ck
	@Override
	public boolean savePromoCode(String code) {
		if(this.promocodes.add(code)){
	        PrintWriter pWriter = null; 
	        try { 
	            pWriter = new PrintWriter(new BufferedWriter(new FileWriter("promocodes.txt"))); 
	            // Speichere alle promocodes der Liste "promocodes" in einer neuen Text Datei "promocodes.txt"
	            for(int i = 0; i < promocodes.size();i++)
	            	pWriter.println(promocodes.get(i)); 
	        } catch (IOException ioe) { 
	            ioe.printStackTrace(); 
	        } finally { 
	            if (pWriter != null){ 
	                pWriter.flush(); 
	                pWriter.close(); 
	            } 
	        } 
	        return true;
		}
		return false;
	}

	@Override
	public boolean isPromoCode(String code) {
		return this.promocodes.contains(code);
	}

}
