package gui;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblPromocode;
	private JLabel lblImage;
	private ImageIcon image;
	
		//Frame erzeugen
	public PromocodeGUI(final IPromoCodeLogic logic) {
		
		//Frame ver�ndern
		
		setTitle("Promotioncode-Generator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 331);
		
			//Frame auf dem Bildschirm zentrieren
			setLocation((Toolkit.getDefaultToolkit().getScreenSize().width  - getSize().width) / 2, 
				(Toolkit.getDefaultToolkit().getScreenSize().height - getSize().height) / 2); 
		
		//Erstellen des Border-Layout
		contentPane = new JPanel();
		contentPane.setBackground(Color.black);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		image = new ImageIcon("napstablook.jpg");
		lblImage = new JLabel(image);
		contentPane.add(lblImage, BorderLayout.EAST);
		// Promocode Mitte
		lblPromocode = new JLabel("");
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setFont(new Font("Calibri", Font.BOLD, 18));
		lblPromocode.setForeground(Color.WHITE);
		
		
		contentPane.add(lblPromocode, BorderLayout.CENTER);
		
		//Button Unten
		JButton btnNewPromoCode = new JButton("Generiere");
		btnNewPromoCode.setBackground(Color.WHITE);
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code = logic.getNewPromoCode();
				lblPromocode.setText(code);
			}
		});
		contentPane.add(btnNewPromoCode, BorderLayout.SOUTH);
		
		// Beschriftung Oben
		JLabel lblBeschriftungPromocode = new JLabel("Promotionscodes sind eh �berbewertet .....");
		lblBeschriftungPromocode.setHorizontalAlignment(SwingConstants.CENTER); // Beschriftung zentrieren
		lblBeschriftungPromocode.setFont(new Font("Calibri", Font.BOLD, 14)); // Font ge�ndert und Gr��e
		lblBeschriftungPromocode.setForeground(Color.WHITE);
		contentPane.add(lblBeschriftungPromocode, BorderLayout.NORTH);
		this.setVisible(true);
	}

}
