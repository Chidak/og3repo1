package start;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import gui.PromocodeGUI;
//import gui.PromocodeTUI;
import logic.IPromoCodeLogic;
import logic.PromoCodeLogic;
import data.IPromoCodeData;
import data.PromoCodeData;

public class PromoCodeStart {

	public static void main(String[] args) {
		//Die 3 Schichten verbinden
		IPromoCodeData data = new PromoCodeData();
		IPromoCodeLogic logic = new PromoCodeLogic(data);
		//new PromocodeTUI(logic); 
		new PromocodeGUI(logic);
		new PromoCodeStart();
	
	}
	//MP3 Player
	public	PromoCodeStart() {
		//die Quelle muss ggf. ge�ndert werden 
		//String p = "C:\\Users\\Chidak\\git\\og3repo1\\3SchichtenArchitektur\\Ghost Fight.mp3";
		//String p = "E:\\eclipse\\EclipseGit\\og3repo1\\3SchichtenArchitektur\\Ghost Fight.mp3";
		String p = "Ghost Fight.mp3";
		try {
					FileInputStream in = new FileInputStream(p);
					Player pl = new Player(in);
					pl.play();
			}
			
		catch(JavaLayerException jle){
			jle.printStackTrace();
		}
		catch (FileNotFoundException fnf){
			fnf.printStackTrace();
		}
	}

}
